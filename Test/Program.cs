﻿using Kmeans;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Kmeans.Kmeans kmeans = new Kmeans.Kmeans(
                Enumerable.Range(0, 500).Select(i => new double[] { random.NextDouble(), random.NextDouble() }),
                (p1, p2) => Math.Sqrt(Math.Pow(p1[0] - p2[0], 2) + Math.Pow(p1[1] - p2[1], 2)),
                5,
                saveResult
                );
            //unfortunately doesn't work
            //HierarchicalGrouping hg = new HierarchicalGrouping(
            //    Enumerable.Range(0, 100).Select(i => new double[] { random.NextDouble(), random.NextDouble() }),
            //    (p1, p2) => Math.Pow(p1[0] - p2[0], 2) + Math.Pow(p1[1] - p2[1], 2),
            //    5,
            //    saveResult
            //    );
        }

        static void saveResult(List<Group> result, int index)
        {
            Bitmap bitmap = new Bitmap(200, 200);
            for (int w = 0; w < bitmap.Width; w++)
            {
                for (int h = 0; h < bitmap.Height; h++)
                {
                    bitmap.SetPixel(w, h, Color.Black);
                }
            }
            Color[] colors = new Color[]
            {
                Color.Red,
                Color.Yellow,
                Color.Blue,
                Color.Green,
                Color.Gray
            };
            for (int i = 0; i < result.Count; i++)
            {
                Group group = result[i];
                Color color = colors[i];
                foreach(double[] point in group.Points)
                {
                    bitmap.SetPixel((int)(point[0] * 200), (int)(point[1] * 200), color);
                }
            }
            bitmap.Save("img" + index + ".bmp", ImageFormat.Bmp);
        }
    }
}
