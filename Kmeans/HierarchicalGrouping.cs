﻿//unfortunately doesn't work
/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kmeans
{
    public class HierarchicalGrouping
    {
        public HierarchicalGrouping(IEnumerable<double[]> points, Func<double[], double[], double> distance, int groupsNumber, Action<List<Group>, int> callback)
        {
            int dimensions = points.ElementAt(0).Length;
            if (points.Any(p => p.Length != dimensions)) throw new ArgumentException("Dimensions of points vary", nameof(points));
            if (points.Count() < groupsNumber) throw new InvalidOperationException("Too many groups for points");

            this.Groups = Enumerable.Range(0, groupsNumber).Select(i => new Group() { Center = new double[dimensions] }).ToList();

            //double[][] pointsArray = new double[points.Count()][];
            //for (int i = 0; i < points.Count(); i++)
            //{
            //    pointsArray[i] = points.ElementAt(i);
            //}
            //double[,] distanceMatrix = new double[points.Count(), points.Count()];
            //for (int h = 0; h < points.Count(); h++)
            //{
            //    for (int w = 0; w < points.Count(); w++)
            //    {
            //        distanceMatrix[h, w] = distance(pointsArray[h], pointsArray[w]);
            //    }
            //}
            List<Point> points2 = points.Select(p => new Point() { Data = p, Group = -1 }).ToList();
            List<PointsPair> pairs = new List<PointsPair>();
            foreach (Point p1 in points2)
            {
                foreach (Point p2 in points2)
                {
                    if(p1 != p2) pairs.Add(new PointsPair() { P1 = p1, P2 = p2, Distance = distance(p1.Data, p2.Data) });
                }
            }

            var orderedPairs = pairs.OrderBy(pp => pp.Distance);
            int groupIndex = 0;
            List<Point> pointsWithoutGroup = points2.ToList();
            List<PointsPair> pairsWithoutGroup = pairs.ToList();
            for (int i = 0; i < groupsNumber; i++)
            {
                var pair = orderedPairs.ElementAt(i);
                pair.P1.Group = pair.P2.Group = i;
                pointsWithoutGroup.Remove(pair.P1);
                pointsWithoutGroup.Remove(pair.P2);
                pairsWithoutGroup.RemoveRange(pairsWithoutGroup.Where(pp => pp.P1 == pair.P1 || pp.P2 == pair.P1 || pp.P2 == pair.P2 || pp.P1 == pair.P2 || ));
            }
            for (int i = 0; i < points.Count() - groupsNumber; i++)
            {

            }
        }

        private class PointsPair
        {
            public Point P1 { get; set; }

            public Point P2 { get; set; }

            public double Distance { get; set; }
        }

        private class Point
        {
            public double[] Data { get; set; }

            public int Group { get; set; }
        }

        public List<Group> Groups { get; private set; }
    }
}
*/