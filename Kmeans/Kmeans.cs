﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kmeans
{
    public class Kmeans
    {
        public Kmeans(IEnumerable<double[]> points, Func<double[], double[], double> distance, int groupsNumber, Action<List<Group>, int> callback)
        {
            int dimensions = points.ElementAt(0).Length;
            if (points.Any(p => p.Length != dimensions)) throw new ArgumentException("Dimensions of points vary", nameof(points));
            if (points.Count() < groupsNumber) throw new InvalidOperationException("Too many groups for points");

            this.Groups = Enumerable.Range(0, groupsNumber).Select(i => new Group() { Center = new double[dimensions] }).ToList();
            List<double[]> pointsToGiveAway = points.ToList();
            Random random = new Random();
            for (int i = 0; pointsToGiveAway.Count != 0; i = ++i % groupsNumber)
            {
                int index = random.Next(pointsToGiveAway.Count);
                this.Groups[i].Points.Add(pointsToGiveAway[index]);
                pointsToGiveAway.RemoveAt(index);
            }
            callback(this.Groups, 0);

            for (int i = 0; i < 5; i++)
            {
                foreach (Group group in this.Groups)
                {
                    this.translateCenter(group);
                }
                Dictionary<double[], Group> pointsToGroups = new Dictionary<double[], Group>();
                foreach (Group group in this.Groups)
                {
                    foreach(double[] point in group.Points)
                    {
                        double minDistance = double.MaxValue;
                        Group bestGroup = null;
                        foreach (Group group2 in this.Groups)
                        {
                            double dist = distance(group2.Center, point);
                            if (dist < minDistance)
                            {
                                minDistance = dist;
                                bestGroup = group2;
                            }
                        }
                        pointsToGroups.Add(point, bestGroup);
                    }
                }
                this.Groups.ForEach(g => g.Points.Clear());
                foreach(KeyValuePair<double[], Group> point in pointsToGroups)
                {
                    point.Value.Points.Add(point.Key);
                }
                callback(this.Groups, i + 1);
            }
        }

        private void translateCenter(Group group)
        {
            double[] newCenter = new double[group.Center.Length];
            foreach(double[] point in group.Points)
            {
                for (int i = 0; i < newCenter.Length; i++)
                {
                    newCenter[i] += point[i];
                }
            }
            for (int i = 0; i < newCenter.Length; i++)
            {
                newCenter[i] /= group.Points.Count;
            }
            group.Center = newCenter;
        }
        
        public List<Group> Groups { get; private set; }
    }

    public class Group
    {
        public double[] Center { get; set; }

        public List<double[]> Points { get; set; } = new List<double[]>();
    }
}
